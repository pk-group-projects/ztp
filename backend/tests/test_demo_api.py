def test_create_note_unauthorised(client):
    response = client.post('/notes', data={
        "note": "some random text"
    })

    # Redirection to login page
    assert response.status_code == 302
