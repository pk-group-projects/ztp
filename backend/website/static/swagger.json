openapi: 3.0.3
info:
  title: Flask Application API
  description: API documentation for the Flask application
  version: 1.0.0

servers:
  - url: http://localhost:5000
    description: Local server

paths:
  /login:
    post:
      summary: Log in a user
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  example: "user@example.com"
                password:
                  type: string
                  example: "password123"
      responses:
        '200':
          description: Successful login
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Logged in successfully"
        '401':
          description: Login failed
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Login failed"

  /logout:
    get:
      summary: Log out the current user
      responses:
        '302':
          description: Redirect to login page

  /sign_up:
    post:
      summary: Sign up a new user
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  example: "newuser@example.com"
                name:
                  type: string
                  example: "John"
                password1:
                  type: string
                  example: "password123"
                password2:
                  type: string
                  example: "password123"
      responses:
        '200':
          description: Account created
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Account created"
        '400':
          description: Error during account creation
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Email already exists."
                  category:
                    type: string
                    example: "error"

  /promote/{id}:
    put:
      summary: Promote a user to admin
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: User promoted
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "The user has been promoted!"
        '404':
          description: User not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "No user found!"

  /delete_user/{id}:
    delete:
      summary: Delete a user
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: User deleted
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "The user has been deleted!"
        '404':
          description: User not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "No user found!"

  /show_users:
    get:
      summary: Get all users
      responses:
        '200':
          description: List of users
          content:
            application/json:
              schema:
                type: object
                properties:
                  users:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: integer
                        email:
                          type: string
                        password:
                          type: string
                        first_name:
                          type: string

  /show_user/{id}:
    get:
      summary: Get a single user by ID
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: User details
          content:
            application/json:
              schema:
                type: object
                properties:
                  user:
                    type: object
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
        '404':
          description: User not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "No user found!"

  /:
    get:
      summary: Home page
      responses:
        '200':
          description: Home page
          content:
            text/html:
              schema:
                type: string

    post:
      summary: Add a new note
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                note:
                  type: string
      responses:
        '200':
          description: Note added
          content:
            text/html:
              schema:
                type: string

  /notes:
    get:
      summary: Notes page
      responses:
        '200':
          description: Notes page
          content:
            text/html:
              schema:
                type: string

    post:
      summary: Add a new note
      requestBody:
        required: true
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                note:
                  type: string
      responses:
        '200':
          description: Note added
          content:
            text/html:
              schema:
                type: string

  /courses:
    get:
      summary: Courses page
      responses:
        '200':
          description: Courses page
          content:
            text/html:
              schema:
                type: string

  /courses/{id}:
    get:
      summary: Get a single course by ID
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: Course details
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  description:
                    type: string
                  title:
                    type: string
                  complete:
                    type: boolean
        '404':
          description: Course not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "No courses found!"

    post:
      summary: Add a new course
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                course_id:
                  type: integer
                description:
                  type: string
                title:
                  type: string
                complete:
                  type: boolean
      responses:
        '200':
          description: Course added
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Course added"
        '400':
          description: Failed to add course
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Fail"

    delete:
      summary: Delete a course
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: Course deleted
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "courses item deleted!"
        '404':
          description: Course not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "No courses found!"

  /administration_panel:
    get:
      summary: Administration panel page
      responses:
        '200':
          description: Administration panel page
          content:
            text/html:
              schema:
                type: string

  /delete_note:
    post:
      summary: Delete a note
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                noteId:
                  type: integer
      responses:
        '200':
          description: Note deleted
          content:
            application/json:
              schema:
                type: object
