const { defaults: tsjPreset } = require('ts-jest/presets');

module.exports = {
    testEnvironment: 'jsdom',
    preset: 'ts-jest',
    transform: {
        ...tsjPreset.transform,
        '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest',
        '\\.(png|jpg|jpeg|gif|svg|ttf|woff|woff2)$': 'jest-transform-stub', // Add this line
    },
    moduleNameMapper: {
        '\\.(css|less)$': 'identity-obj-proxy',
    },
    setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
    esModuleInterop: true,
    allowSyntheticDefaultImports: true,
};
