import React, { useState } from 'react'
import './Instructor.css'
import { assets } from '../../assets/assets'
import axios from "axios"
import { toast } from 'react-toastify'

const Instructor = () => {

    const url = "http://127.0.0.1:5000"

    const [message, setMessage] = useState('');
    const [image,setImage] = useState(false)
    const [data, setData] = useState({
        name:"",
        description:"",
        price:"",
        category:"Coding",
    })

    const onChangeHandler = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setData(data=>({...data, [name]:value}))
    }

    const onSubmitHandler = async (event) =>{
        event.preventDefault();
        // const formData = new FormData();
        // formData.append("name", data.name)
        // formData.append("description", data.description)
        // formData.append("price", Number(data.price))
        // formData.append("category", data.category)
        // formData.append("image",image)
        console.log('Current value:', data)
        try{
            const response = await axios.post(url + "/courses", data);
            setMessage(response.data.message);
            if (response.status === 201){
                console.log('Success')
                window.location.reload();
            }else{
                alert('error')
            }
        }catch (error) {
            setMessage(error)
        }
    } 


  return (
    <div className='add'>
        <form className='flex-col' onSubmit={onSubmitHandler}>
            <div className="add-img-upload flex-col">
                <p>Upload Image</p>
                <label htmlFor="image">
                    <img src={image?URL.createObjectURL(image):assets.upload_area} alt="" />
                </label>
                <input onClick={(e)=>setImage(e.target.files[0])} type="file" id="image" hidden required/>
            </div>
            <div className="add-course-name flex-col">
                <p>Course name</p>
                <input name='name' onChange={onChangeHandler} value={data.name} type="text" placeholder='Type here'required/>
            </div>
            <div className="add-course-description flex-col">
                <p>Course description</p>
                <textarea onChange={onChangeHandler} value={data.description} name="description" rows='6' placeholder='Write content here' required></textarea>
            </div>
            <div className="add-category-price">
                <div className="add-category flex-col">
                    <p>Product category</p>
                    <select onChange={onChangeHandler} name="category" >
                        <option value="Coding">Coding</option>
                        <option value="Math">Math</option>
                        <option value="Chemistry">Chemistry</option>
                        <option value="Physics">Physics</option>
                        <option value="Biology">Biology</option>
                        <option value="Test1">Test1</option>
                        <option value="Test2">Test2</option>
                        <option value="Test3">Test3</option>
                    </select>
                </div>
                <div className="add-proce flex-col">
                    <p>Course price</p>
                    <input onChange={onChangeHandler} value={data.price} type="number" name="price" placeholder='$' />
                </div>
            </div>
            <button type='submit' onClick={onSubmitHandler} className='add-button'>ADD</button>
        </form>
      
    </div>
  )
}

export default Instructor
