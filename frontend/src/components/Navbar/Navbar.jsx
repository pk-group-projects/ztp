import React, { useContext, useState } from 'react'
import './Navbar.css'
import { assets } from '../../assets/assets'
import { Link, useNavigate } from 'react-router-dom'
import { StoreContext } from '../../context/StoreContext'

const Navbar = ({setShowLogin}) => {

    const[menu, setMenu] = useState("menu")

    const{getTotalCartAmount, token, setToken} = useContext(StoreContext)

    const navigate = useNavigate()
    const logOut = ()=>{
      localStorage.removeItem("token");
      setToken("");
      navigate("/")
    }

    const handleSearchSubmit = async (event) => {
    event.preventDefault();
    if (!searchQuery) {
      toast.error("Please enter a search query");
      return;
    }

    try {
      const response = await axios.get(`${url}/courses`, {
        params: { search_value: searchQuery },
        headers: { Authorization: `Bearer ${token}` }
      });
      // Handle the response data as needed (e.g., set a state to display the courses)
      console.log(response.data.courses);
    } catch (error) {
      console.error("Error searching courses:", error);
      toast.error("An error occurred while searching for courses.");
    }
  };


  return (
    <div className='navbar'>
      <Link to='/'><img src={assets.logo} alt="" className="logo" /></Link>
      <ul className="navbar-menu">
      <Link to='/' onClick={()=>setMenu("home")} className={menu==="home"?"active":""}>Home</Link>
      <a href='#explore-menu' onClick={()=>setMenu("menu")} className={menu==="menu"?"active":""}>Courses</a>
      <a href='#app-download'onClick={()=>setMenu("mobile-app")} className={menu==="mobile-app"?"active":""}>Mobile App</a>
      <a href='#contact-us'onClick={()=>setMenu("contact-us")} className={menu==="contact-us"?"active":""}>Contact Us</a>
      </ul>
      <div className="navbar-right">
        <img src={assets.search_icon} alt="" />
        {token !== "" && (<Link to='/instructor'><img src={assets.profile_icon} alt="" /></Link>)}
        <div className="navbar-search-icon">
           <Link to='/cart'><img src={assets.basket_icon} alt="" /></Link>
           <div className={getTotalCartAmount()===0?"":"dot"}>
           </div>
           {!token || token ==="" ?<button onClick={()=>setShowLogin(true)}>Sing In</button>:<div className='navbar-profile'>
              <img src={assets.profile_icon} alt="" />
              <ul className="navbar-profile-dropdown">
                <Link to='/my-courses'><li><img src={assets.basket_icon} alt="" /><p>My Courses</p></li></Link>
                <hr/>
                <Link to='/my-account'><li><img src={assets.basket_icon} alt="" /><p>My Account</p></li></Link>
                <hr/>
                <li onClick={logOut}><img src={assets.logout_icon} alt="" /><p>Log Out</p></li>
              </ul>
            </div>}
           
           </div>
      </div>
    </div>
  )
}

export default Navbar
