import React from 'react'
import './Header.css'

const Header = () => {
  return (
    <div className='header'>
      <div className="header-contents">
        <h2>Gain knowledge with our online courses</h2>
        <p>Explore world with us!</p>
        <button>View courses</button>
      </div>
    </div>
  )
}

export default Header
