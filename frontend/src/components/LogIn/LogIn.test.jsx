import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import LogIn from './LogIn';
import { StoreContext } from '../../context/StoreContext';
import { BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';

jest.mock('axios');

const mockSetShowLogin = jest.fn();
const mockSetToken = jest.fn();
const mockUrl = "http://127.0.0.1:5000";

const renderLogIn = () => {
  return render(
    <StoreContext.Provider value={{ url: mockUrl, setToken: mockSetToken }}>
      <Router>
        <LogIn setShowLogin={mockSetShowLogin} />
        <ToastContainer />
      </Router>
    </StoreContext.Provider>
  );
};

describe('LogIn Component', () => {
  test('renders Login button', () => {
    render(
      <StoreContext.Provider value={{ url: mockUrl, setToken: mockSetToken }}>
        <Router>
          <LogIn setShowLogin={mockSetShowLogin} />
          <ToastContainer />
        </Router>
      </StoreContext.Provider>
    );

    const linkElement = screen.getByText(/Create account/i);
    expect(linkElement).toBeInTheDocument();
  });
});
