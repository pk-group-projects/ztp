import React, { useContext, useState } from 'react'
import './LogIn.css'
import { assets } from '../../assets/assets'
import { StoreContext } from '../../context/StoreContext'
import axios from "axios"
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

const LogIn = ({setShowLogin}) => {

  const {url,setToken} = useContext(StoreContext);
    const history = useNavigate();

    const [currState,setCurrState] = useState("LogIn")
    const [data,setData] = useState({
      email:"",
      name:"",
      password1:"",
      role:"",
    })
    const [message, setMessage] = useState('');

    const onChangeHandler = (event) => {
      const name = event.target.name;
      const value = event.target.value;
      setData(data=>({...data,[name]:value}))
      toast.success(response.data.message)
    }

    const onLogIn = async (event) => {
      event.preventDefault();
      let newUrl = url;
      const roleForm = document.getElementById('role-form');
      const roleSelected = roleForm.querySelector('input[name="role"]:checked');

      if (currState!=="LogIn" && !roleSelected) {
        event.preventDefault();
        alert("Please select a role.");
      }
      if (currState==="LogIn"){
        newUrl+="/login"
      }
      else{
        newUrl += "/sign_up"
      }
      
      console.log('Current value:', data)
      try {
        const response = await axios.post(newUrl,data);
        setMessage(response.data.message);
        if (response.status === 200){
          setToken(response.data.token);
          localStorage.setItem("token",response.data.token)
          setShowLogin(false)
          //history.push("/")
        }
        else{
          alert('error1')
        }
      } catch (error) {
        setMessage('Wrong credentials')
      }
      window.location.reload();
    }

  return (
    <div className='login'>
      <form onSubmit={onLogIn} className="login-container">
        <div className="login-title">
            <h2>{currState}</h2>
            <img onClick={()=>setShowLogin(false)} src={assets.cross_icon} alt="" />
        </div>
        <div className="login-inputs">
            {currState==="LogIn"?<></>:<input name='name' onChange={onChangeHandler} value={data.name} type="text" placeholder='Name' required/>}
            <input name='email' onChange={onChangeHandler} value={data.email} type="email" placeholder='Email' required/>
            <input name='password1' onChange={onChangeHandler} value={data.password1} type="password" placeholder='Password' required/>
        </div>
        <div id="role-form" className="login-condition">
            {currState==="LogIn"?<></>:<input type="radio" id="student" name="role" value="student" onChange={onChangeHandler}/>}
            {currState==="LogIn"?<></>:<p>Student</p>}
            {currState==="LogIn"?<></>:<input type="radio" id="instructor" name="role" value="instructor" onChange={onChangeHandler}/>}
            {currState==="LogIn"?<></>:<p>Instructor</p>}
        </div>
        
        <button type="submit">{currState==="Sign Up"?"Create account":"LogIn"}</button>
        <div className="login-condition">
            {currState==="LogIn"?<></>:<input type="checkbox" required />}
            {currState==="LogIn"?<></>:<p>Terms and conditions</p>}
        </div>
        
        
        {currState==="LogIn"?<p>Create account <span onClick={()=>setCurrState("Sign Up")}> Click here</span></p>:<p>Already have an account? <span onClick={()=>setCurrState("LogIn")}>Log In here</span></p>}
        <toast className="error">{message && <p>{message}</p>}</toast>
      </form>
    </div>
    
  )
}
        
export default LogIn
